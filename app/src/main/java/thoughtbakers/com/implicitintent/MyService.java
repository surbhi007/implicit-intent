package thoughtbakers.com.implicitintent;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.Enumeration;
import java.util.Vector;

import static thoughtbakers.com.implicitintent.MainActivity.text;

public class MyService extends Service  {

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e("oncreate","oncreateservice");
      new SeconWorker().execute();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {

        Log.e("bind method","onbind called");
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.e("unbind method","unbind called");
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        new SeconWorker().execute();

        Handler mainHandler = new Handler(Looper.getMainLooper());


        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                Thread.currentThread().setName("Saurabh");
              //  System.out.println(" Thread: "+Thread.currentThread());
                Log.e("thread",Thread.currentThread().toString());
                int a=5,b=5,c;
                c=a+b;
                MainActivity.text.setText(String.valueOf(c));
                Log.e("sum", String.valueOf(c));
                Vector vector = new Vector();
                vector.add("abc");
                vector.add("cde");
                vector.add("efg");
                vector.add("dgf");
                Enumeration enumeration = vector.elements();

                while (enumeration.hasMoreElements()){

                    Log.e("element", String.valueOf(enumeration.nextElement()));
                }


            } // This is your code
        };
        mainHandler.post(myRunnable);



        Toast.makeText(this,"Servicestart",Toast.LENGTH_SHORT).show();



        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    public class SeconWorker extends AsyncTask<String,String,String>{




        @Override
        protected String doInBackground(String... strings) {


            //System.out.println("workerthread"+Thread.currentThread());
            Log.e("async","doinbackground");
            Log.e("asynthread",Thread.currentThread().toString());


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("async","preexecute");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("async","postexecute");
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            Log.e("async","progressupdate");
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
            Log.e("async","oncacel");
        }
    }
}
