package thoughtbakers.com.implicitintent;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

public class WorkerThread extends IntentService {


    public WorkerThread() {
        super("My Intent");

    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        Log.e("intent",intent.toString());

    }
}
