package thoughtbakers.com.implicitintent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE = "EASYREWARDZ";
    public static  final String TABLE_NAME = "REGISTER";
    public static final String COL_3 = "ID";
    public  static final String COL_1 = "NAME";
    public static final String COL_2 = "PHONE";


    public DataBaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DataBaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL( "create table " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT ,NAME TEXT, PHONE INTEGER)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);

        onCreate(sqLiteDatabase);

    }

    public  boolean insertData(String name, String phone){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,name);
        contentValues.put(COL_2,phone);
        long result = db.insert(TABLE_NAME, null,contentValues);

        if (result == -1)
            return false;
        else
            return true;
    }

    public Cursor getAllData(){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select *from "+ TABLE_NAME,null);

        return cursor;
    }

    public  boolean updateDatabase(String id,String name, String phone){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,name);
        contentValues.put(COL_2,phone);
        contentValues.put(COL_3,id);
        db.update(TABLE_NAME,contentValues,"ID= ?",new String[]{ id});

        return  true;
    }

    public Integer deleteData(String id){

        SQLiteDatabase db = this.getWritableDatabase();

        return  db.delete(TABLE_NAME,"ID =?",new String[]{id});


    }


}
