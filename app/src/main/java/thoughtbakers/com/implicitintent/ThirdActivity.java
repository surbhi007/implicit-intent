package thoughtbakers.com.implicitintent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ThirdActivity extends MainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
    }

    @Override
    public void sum() {

    }
}
