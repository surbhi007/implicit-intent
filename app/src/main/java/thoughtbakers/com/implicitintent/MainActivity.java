package thoughtbakers.com.implicitintent;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public  class MainActivity extends Activity implements View.OnClickListener,Abs{

    private Button btn;
    private EditText editText,edit;
    DataBaseHelper dataBaseHelper;
    static TextView text;
    boolean result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this,"on Create",Toast.LENGTH_SHORT).show();
        editText = (EditText)findViewById(R.id.edit);
        edit  = (EditText)findViewById(R.id.edit1);
        text = (TextView)findViewById(R.id.text);

        Intent intent=new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://www.tutlane.com"));
        startActivity(intent);
        /*Intent intent=new Intent(Intent.ACTION_VIEW);
        PackageManager managerclock = getPackageManager();
        intent = managerclock.getLaunchIntentForPackage("com.whatsapp");
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        startActivity(intent);*/



        startService(new Intent(this,MyService.class));

          //text.setText("");

        btn = (Button)findViewById(R.id.btn);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId() ) {
            case R.id.btn:

               /* String s = editText.getText().toString();
                String s2  = edit.getText().toString();

                if ( s.equals("") && s2.equals("")){

                    Toast.makeText(this,"fill first",Toast.LENGTH_SHORT).show();
                }
                else {


                   result = dataBaseHelper.insertData(s,s2);

                   if (result == true){

                       Toast.makeText(this,"inserted" ,Toast.LENGTH_SHORT).show();
                   }else{

                       Toast.makeText(this,"not inserted" ,Toast.LENGTH_SHORT).show();
                   }

                }*/

                  startActivity(new Intent(MainActivity.this,SecondActivity.class));

                 break;

            default:
                break;
        }


    }

   // public abstract void sum();

    @Override
    public void sum() {

        startActivity(new Intent(this,BlankFragment.class));
        Toast.makeText(this,"do implementation of sum here",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void plus() {

        Toast.makeText(this,"do implementation of plus here",Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this,"on start",Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Toast.makeText(this,"on Restart",Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this,"on Resume",Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this,"on pause",Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this,"on stop",Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this,"on Destroy",Toast.LENGTH_SHORT).show();

    }
}
